
# AdminGLT 
参考AdminLTE,并增加诸多修改，提供了诸多的强大的可以重新组合的UI组件,网站管理后台，网站会员中心，CMS，CRM，OA等等，当然，您也可以对她进行深度定制，以做出更强系统
# 最新更新
- 2018-1-25 更新
	- 调整日程安排 demo，增加删除和编辑项
	- 增加表格和Tree合并页面
- 2017-12-25 更新
	- 增加wizard demo，具体参照UI forms/wizards.html页面
- 2017-12-19 更新
	- 增加图片上传demo，具体参照UI forms/uploader.html页面
- 2017-11-23 更新
	- 增加顶部浮动操作区域，具体参照UI Elements/Buttons.html页面
- 2017-11-17 更新
	- 增加图片点击查看的Viewer插件
- 2017-11-02 更新
	- 提供前端Less文件
- 2017-10-20 更新adminlte.js
	- 修改点击菜单时菜单不联动的问题
- 2017-10-17 更新addTabs脚本 
	- 增加加载iframe时的缓冲提醒。
	- 修改窗口缩小时，tab标签换行的问题
	- 增加双击tab标签刷新当前页的功能（注意跨域）

# 演示地址

[http://lvzhig.gitee.io/adminglt/](http://lvzhig.gitee.io/adminglt/)

# 功能说明

修改部分如下：

- 采用adminLTE模版做的修改，原adminLTE的样式和功能都适用。
- 左侧菜单做了修改，支持多Tab窗口。
- 底部增加功能菜单，可自己修改，目前显示项目介绍、锁定页、全屏显示、退出按钮
- 右上角用户显示部分做了修改，增加了修改头像、修改密码等功能。
- 修改右侧Tab页面，固定了其页面展示功能。
- 修改了其表格显示插件
- 增加页内下拉功能，适用简单功能的添加和修改
- 修改了TreeView插件
- 右下角增加在线聊天功能，可显示好友列表及在线、离线状态，并可实现在线聊天和广播
- 增加max-length插件，显示输入字数显示
- 增加iconpicker插件，并修改其支持fa-字符
- 增加toastr 提示框

其他修改后续更新后说明


# 使用说明
在`<head>...</head>`间增加

    <link href="plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="plugins/bootstrap-addtabs/bootstrap.addtabs.min.css" rel="stylesheet" />
    <link href="plugins/layer/build/skin/default/layer.css" rel="stylesheet" />
    <link href="plugins/toastr/toastr.min.css" rel="stylesheet" />
    <link href="plugins/custombox/dist/custombox.min.css" rel="stylesheet" />
    <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="plugins/Ionicons/css/ionicons.min.css" rel="stylesheet" />
    <link href="plugins/animate/animate.min.css" rel="stylesheet" />
    <link href="plugins/viewer/viewer.min.css" rel="stylesheet" />
    <link href="plugins/adminLTE/dist/css/AdminLTE.min.css" rel="stylesheet" />
    <link href="plugins/adminLTE/dist/css/skins/all-skins.min.css" rel="stylesheet" />
	
在文档尾部增加脚本引用

    <script src="plugins/jquery/dist/jquery.min.js"></script>
    <script src="plugins/layer/build/layer.js"></script>
    <script src="plugins/toastr/toastr.min.js"></script>
    <script src="plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/bootstrap-addtabs/bootstrap.addtabs.min.js"></script>
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="plugins/screenfull/dist/screenfull.min.js"></script>
    <script src="plugins/custombox/dist/custombox.min.js"></script>
    <script src="plugins/viewer/viewer.min.js"></script>
    <script src="plugins/adminLTE/dist/js/adminlte.min.js"></script>
    <script src="dist/gtl.mini.js"></script>

其他子页面，在
    `<body class="hold-transition skin-blue sidebar-mini" style="background-color:#ecf0f5"><section>...</section></body>`

处增加具体要显示的内容



#有图有真相

在线通知功能

![](https://static.oschina.net/uploads/space/2017/1011/141026_m80P_2689711.gif)
其他

![](https://static.oschina.net/uploads/space/2017/1010/142906_8xG6_2689711.png)

![](https://static.oschina.net/uploads/space/2017/1010/142922_z8Jz_2689711.png)

![](https://static.oschina.net/uploads/space/2017/1010/142937_jeWT_2689711.png)

![](https://static.oschina.net/uploads/space/2017/1010/143152_3tbo_2689711.png)

![](https://static.oschina.net/uploads/space/2017/1010/142955_VjIV_2689711.png)




# 授权信息

[MIT](https://opensource.org/licenses/MIT "MIT")

----------


# 联系我们

在线讨论请加群

![](https://static.oschina.net/uploads/space/2017/1011/131225_NKWH_2689711.png)